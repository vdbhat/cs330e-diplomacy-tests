from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_solve#, collatz_eval, collatz_print, collatz_solve, collatz_range_helper


import random
# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):

    def test_solve1(self):
        r = StringIO('A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London\nE Dallas Hold\nF Houston Support B')
        w = StringIO()

        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), 'A [dead]\nB Madrid\nC [dead]\nD [dead]\nE Dallas\nF Houston')

    def test_solve2(self):
        r = StringIO('A Madrid Move London\nB Barcelona Move Madrid\nC London Move Barcelona')
        w = StringIO()

        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), 'A London\nB Madrid\nC Barcelona')

    def test_solve3(self):
        r = StringIO('A Madrid Move London\nB London Move Dallas\nC Dallas Move Austin\nD Austin Hold')
        w = StringIO()

        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), 'A London\nB Dallas\nC [dead]\nD [dead]')

    def test_solve4(self):
        r = StringIO('A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Support A\nE Dallas Move Madrid')
        w = StringIO()

        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), 'A [dead]\nB [dead]\nC London\nD Austin\nE [dead]')

    def test_solve5(self):  
        r = StringIO('Z Austin Hold\nX Houston Move Austin\nY Dallas Move Houston')
        w = StringIO()

        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), 'X [dead]\nY Houston\nZ [dead]')

    def test_solve6(self):  
        r = StringIO('P Austin Hold')
        w = StringIO()

        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), 'P Austin')

if __name__ == "__main__":
    main()


""" #pragma: no cover
$ coverage run --branch TestDiplomacy.py >  TestDiplomacy.out 2>&1
""" 