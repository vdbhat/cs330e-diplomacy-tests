from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve

class TestDiplomacy(TestCase):
	# ----
	# read
	# ----

	def test_read_1(self):
		s = "A Madrid Hold\nB Barcelona Move Madrid\nC London Support B"
		armies, cities, support = diplomacy_read(s)
		expected_armies = {"A": "Madrid", "B": "Barcelona", "C": "London"}
		expected_cities = {"Madrid": ["A", "B"], "London": ["C"]}
		expected_support = {"A": 1, "B": 2, "C": 1}
		self.assertEqual(armies, expected_armies)
		self.assertEqual(cities, expected_cities)
		self.assertEqual(support, expected_support)
	
	def test_read_2(self):
		s = "A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London"
		armies, cities, support = diplomacy_read(s)
		expected_armies = {"A": "Madrid", "B": "Barcelona", "C": "London", "D": "Austin"}
		expected_cities = {"Madrid": ["A", "B"], "London": ["C", "D"]}
		expected_support = {"A": 1, "B": 1, "C": 1, "D": 1}
		self.assertEqual(armies, expected_armies)
		self.assertEqual(cities, expected_cities)
		self.assertEqual(support, expected_support)
	
	def test_read_3(self):
		s = "A NewYork Move Seattle\nB Seattle Move NewYork\nC Miami Move Austin\nD Austin Support Miami\nE Honululu Support D"
		armies, cities, support = diplomacy_read(s)
		expected_armies = {"A": "NewYork", "B": "Seattle", "C": "Miami", "D": "Austin", "E": "Honululu"}
		expected_cities = {"Seattle": ["A"], "NewYork": ["B"], "Austin": ["D", "C"], "Honululu": ["E"]}
		expected_support = {"A": 1, "B": 1, "C": 1, "D": 2, "E": 1}
		self.assertEqual(armies, expected_armies)
		self.assertEqual(cities, expected_cities)
		self.assertEqual(support, expected_support)
	
	# ----
	# eval
	# ----

	def test_eval_1(self):
		armies = {"A": "Madrid", "B": "Barcelona", "C": "London"}
		cities = {"Madrid": ["A", "B"], "London": ["C"]}
		support = {"A": 1, "B": 2, "C": 1}
		armies = diplomacy_eval(armies, cities, support)
		expected_armies = {"A": "[dead]", "B": "Madrid", "C": "London"}
		self.assertEqual(armies, expected_armies)
	
	def test_eval_2(self):
		armies = {"A": "Madrid", "B": "Barcelona", "C": "London", "D": "Austin"}
		cities = {"Madrid": ["A", "B"], "London": ["C", "D"]}
		support = {"A": 1, "B": 1, "C": 1, "D": 1}
		armies = diplomacy_eval(armies, cities, support)
		expected_armies = {"A": "[dead]", "B": "[dead]", "C": "[dead]", "D": "[dead]"}
		self.assertEqual(armies, expected_armies)
	
	def test_eval_3(self):
		armies = {"A": "NewYork", "B": "Seattle", "C": "Miami", "D": "Austin", "E": "Honululu"}
		cities = {"Seattle": ["A"], "NewYork": ["B"], "Austin": ["D", "C"], "Honululu": ["E"]}
		support = {"A": 1, "B": 1, "C": 1, "D": 2, "E": 1}
		armies = diplomacy_eval(armies, cities, support)
		expected_armies = {"A": "Seattle", "B": "NewYork", "C": "[dead]", "D": "Austin", "E": "Honululu"}
		self.assertEqual(armies, expected_armies)
	
	# -----
	# print
	# -----

	def test_print_1(self):
		w = StringIO()
		armies = {"A": "[dead]", "B": "Madrid", "C": "London"}
		diplomacy_print(w, armies)
		expected_print = "A [dead]\nB Madrid\nC London\n"
		self.assertEqual(w.getvalue(), expected_print)
	
	def test_print_2(self):
		w = StringIO()
		armies = {"A": "[dead]", "B": "[dead]", "C": "[dead]", "D": "[dead]"}
		diplomacy_print(w, armies)
		expected_print = "A [dead]\nB [dead]\nC [dead]\nD [dead]\n"
		self.assertEqual(w.getvalue(), expected_print)
	
	def test_print_3(self):
		w = StringIO()
		armies = {"A": "Seattle", "B": "NewYork", "C": "[dead]", "D": "Austin", "E": "Honululu"}
		diplomacy_print(w, armies)
		expected_print = "A Seattle\nB NewYork\nC [dead]\nD Austin\nE Honululu\n"
		self.assertEqual(w.getvalue(), expected_print)
	
	# -----
	# solve
	# -----

	def test_solve_1(self):
		r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B")
		w = StringIO()
		diplomacy_solve(r, w)
		expected_print = "A [dead]\nB Madrid\nC London\n"
		self.assertEqual(w.getvalue(), expected_print)
	
	def test_solve_2(self):
		r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London")
		w = StringIO()
		diplomacy_solve(r, w)
		expected_print = "A [dead]\nB [dead]\nC [dead]\nD [dead]\n"
		self.assertEqual(w.getvalue(), expected_print)
	
	def test_solve_3(self):
		r = StringIO("A NewYork Move Seattle\nB Seattle Move NewYork\nC Miami Move Austin\nD Austin Support Miami\nE Honululu Support D")
		w = StringIO()
		diplomacy_solve(r, w)
		expected_print = "A Seattle\nB NewYork\nC [dead]\nD Austin\nE Honululu\n"
		self.assertEqual(w.getvalue(), expected_print)

if __name__ == "__main__": # pragma: no cover
	main()