# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import read, diplomacy_eval, diplomacy_print, diplomacy_solve

# -----------
# TestDiplomacy
# -----------

class TestDiplomacy (TestCase):

    # ----
    # solve
    # ----

    def test_solve_1(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B")        
        w = StringIO()

        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC London\n")

    def test_solve_2(self): 
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\n")
        w = StringIO()

        diplomacy_solve(r, w) 
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\n")

    def test_solve_3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London")
        w = StringIO()

        diplomacy_solve(r, w) 
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

    def test_solve_4(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B")
        w = StringIO()

        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Paris\n")

    # --- 
    # read 
    # ---
    def test_read_1(self): 
        move = "A Madrid Hold\n"
        processed_move = read(move)
        self.assertEqual(processed_move, ["A", "Madrid", "Hold"]) 

    def test_read_2(self): 
        move = "B Barcelona Move Madrid\n"
        processed_move = read(move) 
        self.assertEqual(processed_move, ["B", "Barcelona", "Move", "Madrid"]) 

    def test_read_3(self): 
        move = "C London Support B\n"
        processed_move = read(move) 
        self.assertEqual(processed_move, ["C", "London", "Support", "B"]) 

    def test_read_4(self): 
        move = "D Paris Support B\n"
        processed_move = read(move) 
        self.assertEqual(processed_move, ["D", "Paris", "Support", "B"]) 




    # --- 
    # eval 
    # --- 
    def test_eval_1(self): 
        moves = [["A", "Madrid", "Hold"], ["B", "Barcelona", "Move", "Madrid"], ["C", "London", "Support", "B"]] 
        result = diplomacy_eval(moves)

        self.assertEqual(result, [("A", "[dead]"), ("B", "Madrid"), ("C", "London")])

    def test_eval_2(self): 
        moves = [["A", "Madrid", "Hold"], ["B", "Barcelona", "Move", "Madrid"]] 
        result = diplomacy_eval(moves)

        self.assertEqual(result, [("A", "[dead]"), ("B", "[dead]")])

    def test_eval_3(self): 
        moves = [["A", "Madrid", "Hold"], ["B", "Barcelona", "Move", "Madrid"], ["C", "London", "Support", "B"], ["D", "Austin", "Move", "London"]] 
        result = diplomacy_eval(moves)

        self.assertEqual(result, [("A", "[dead]"), ("B", "[dead]"), ("C", "[dead]"), ("D", "[dead]")])

    def test_eval_4(self): 
        moves = [["A", "Madrid", "Hold"], ["B", "Barcelona", "Move", "Madrid"], ["C", "London", "Move", "Madrid"], ["D", "Paris", "Support", "B"]] 
        result = diplomacy_eval(moves)

        self.assertEqual(result, [("A", "[dead]"), ("B", "Madrid"), ("C", "[dead]"), ("D", "Paris")])



    # ---
    # print 
    # --- 
    def test_print_1(self): 
        w = StringIO()
        diplomacy_print(w, ("A", "[dead]")) 

        self.assertEqual(w.getvalue(), "A [dead]\n")

    def test_print_2(self): 
        w = StringIO()
        diplomacy_print(w, ("B", "Madrid")) 

        self.assertEqual(w.getvalue(), "B Madrid\n")

    def test_print_3(self): 
        w = StringIO() 
        diplomacy_print(w, ("C", "Tokyo"))

        self.assertEqual(w.getvalue(), "C Tokyo\n")

    def test_print_4(self): 
        w = StringIO()
        diplomacy_print(w, ("D", "Paris"))

        self.assertEqual(w.getvalue(), "D Paris\n") 

""" UNNECESSARY UNIT INNER FUNCTION TESTS
    # --- 
    # support 
    # --- 
    def test_support_1(self): 
        pass 

    def test_support_2(self): 
        pass 

    def test_support_3(self): 
        pass 

    def test_support_4(self): 
        pass

    # --- 
    # location 
    # --- 
    def test_location_1(self): 
        pass 

    def test_location_2(self): 
        pass 

    def test_location_3(self): 
        pass 

    def test_location_4(self): 
        pass 

    # --- 
    # check support 
    # --- 

    def test_check_1(self): 
        pass 

    def test_check_2(self): 
        pass 

    def test_check_3(self): 
        pass 

    def test_check_4(self): 
        pass 
"""

# ----
# main
# ----
if __name__ == "__main__":
    main()